# This project is meant for use with
- VSCode
- PlatformIO (an embedded dependency manager)
- ESP32 feather board in Arduino environment

# Repository
- https://gitlab.com/zakkhoyt/HomeKitESP32

# Hardware
- [Huzzah32 ESP32 Feather PCB](https://github.com/adafruit/Adafruit-HUZZAH32-ESP32-Feather-PCB)
  - [Adafruit Downloads](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/downloads)
  - [Espressif Docs](https://www.espressif.com/en/products/socs/esp32)
- [NeoPixel](https://www.aliexpress.us/item/3256801697436853.html)
  - On AliExpress, these are the SK6812 RGBWW model
  - After trail/error, these seem to be of the NEO_GRBW variant when using with AdaNeoPixel library. 

# Pinout
- Using VSCode/PlatformIO, right click on any pin defined in the source (EX: `1`, `A1`, `DAC1`, `TX`, etc...) -> `Go to definition`.
- [Board PDF](https://github.com/adafruit/Adafruit-HUZZAH32-ESP32-Feather-PCB/blob/master/Adafruit%20HUZZAH32%20ESP32%20Feather%20Pinout.pdf)
- [Board product page](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/pinouts)
- Local pinout header file located: `~/.platformio/packages/framework-arduinoespressif32/variants/feather_esp32/pins_arduino.h`


# EEPROM / Flash
* [Example](https://randomnerdtutorials.com/esp32-flash-memory/)