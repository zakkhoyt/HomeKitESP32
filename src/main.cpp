// 
//  main.cpp
// 
//  Written by Zakk Hoyt: 11/2022
//  See $/README.md for project information. 
// 

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <Wifi.h>
#include <ESP32HomeKit.h>

// Servo 
#include <ESP32Servo.h> 

// Stepper + shield
#include <SPI.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_MotorShield.h>

// EEPROM / Flash
#include <EEPROM.h>


// *** ESP_LOGx vars
// Configure platform.ini: https://community.platformio.org/t/how-to-set-up-log-level-to-be-able-to-debug-the-esp32/8278/2
// Define TAG here if needed. 
// https://docs.platformio.org/en/latest/tutorials/espressif32/espidf_debugging_unit_testing_analysis.html?utm_source=docs.espressif.com#debugging-the-firmware
static const char *TAG = "ZK";

// *** WiFi vars
const char *ssid = "PiedPiper";
const char *password = "1001001001";

// *** Servo vars
const int servo_pin = T7;
Servo servo;
uint16_t servo_angle = 0; // (0-180)


// *** LED vars
const int led_pin = A0;
const int led_pixel_count = 8;

bool is_fan_on = false;
uint16_t fan_speed = 0;

bool is_bulb_on = false;
uint16_t hue = 0;
uint8_t saturation = 0;
uint8_t brightness = 0;

Adafruit_NeoPixel pixels(
	led_pixel_count, 
	led_pin, 
	NEO_GRBW + NEO_KHZ800
);

// *** ADC / Potentiometer vars
const uint8_t adc_pin = A2;
const int adc_max = 4096;
uint16_t adc_value = 0;
uint16_t adc_angle = 0; // (0-180)

// *** Stepper / Shield

// TODO: how to validate this at start? Using flash
uint16_t stepper_angle = 0; // 0 - 270 (Or what ever the max angle is)
int16_t stepper_delta = 0;
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_StepperMotor *myMotor = AFMS.getStepper(200, 2);

// *** EEPROM
#define EEPROM_SIZE 32
size_t flash_address = 0x0;


// ------------ Global Functions (LED) ------------

void writeHSV(
	uint16_t hue, 
	uint8_t saturation,
	uint8_t brightness
) {
	
	pixels.clear(); 
	pixels.setPixelColor(0, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(1, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(2, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(3, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(4, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(5, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(6, pixels.ColorHSV(hue, saturation, brightness));
	pixels.setPixelColor(7, pixels.ColorHSV(hue, saturation, brightness));
	pixels.show();

	ESP_LOGD(TAG, "--- Writing HSV %d %d %d Angle %d", hue, saturation, brightness, servo_angle);
}

void writeFan(
	bool is_fan_on,
	uint16_t fan_speed
) {

	// Servo
	servo_angle = map(
		fan_speed,
		0, 100, 
		180, 0
	);
	servo.write(servo_angle);

	// Stepper
	if(is_fan_on) 
	{
		myMotor->step(
			abs(stepper_delta), 
			stepper_delta > 0 ? FORWARD : BACKWARD, 
			DOUBLE
		);
		// Update stepper_angle
		// TOOD: Write stepper_angle to flash
		stepper_angle = stepper_angle + stepper_delta;
		ESP_LOGD(TAG, "--- Writing Fan Postion stepper_delta: %d stepper_angle: %d", stepper_delta, stepper_angle);
	}
	else 
	{
		// Reverse to 0
		myMotor->step(
			stepper_angle, 
			BACKWARD, 
			DOUBLE
		);
		// TODO: Write stepper_angle to flash
		stepper_angle = 0;
		ESP_LOGD(TAG, "--- Writing Fan Position to off stepper_delta: %d stepper_angle: %d", stepper_delta, stepper_angle);
	}
}

// ------------ Global Functions (HomeKit) ------------

/* Mandatory identify routine for the accessory.
 * In a real accessory, something like LED blink should be implemented
 * got visual identifiTAGion
 */
static int identify(
	hap_acc_t *ha
) {
	ESP_LOGI(TAG, "Accessory identified");
	// Red, green, blue, white
	writeHSV(0, 255, 255);
	delay(250);
	writeHSV(21823, 255, 255);
	delay(250);
	writeHSV(43646, 255, 255);
	delay(250);
	writeHSV(0, 0, 255);
	delay(250);

	writeHSV(0, 255, 192);
	delay(250);
	writeHSV(0, 255, 128);
	delay(250);
	writeHSV(0, 255, 64);
	delay(250);
	writeHSV(0, 0, 0);
	return HAP_SUCCESS;
}

/* A callback for handling a read on the characteristic
 * This should read from hardware.
 * Read routines are generally not required as the value is available with th HAP core
 * when it is updated from write routines. For external triggers (like fan switched on/off
 * using physical button), accessories should explicitly call hap_char_update_val()
 * instead of waiting for a read request.
 */
static int bulb_read(
	hap_char_t *hc, 
	hap_status_t *status_code, 
	void *serv_priv, 
	void *read_priv
) {
	ESP_LOGI(TAG, "--------------  Fan Read called");

	if (hap_req_get_ctrl_id(read_priv))
	{
		ESP_LOGI(TAG, "Received read from %s", hap_req_get_ctrl_id(read_priv));
	}
	//if (!strcmp(hap_char_get_type_uuid(hc), HAP_CHAR_UUID_ROTATION_DIRECTION))
	if (!strcmp(hap_char_get_type_uuid(hc), HAP_CHAR_UUID_HUE))
	{
		/* Read the current value, toggle it and set the new value.
        * A separate variable should be used for the new value, as the hap_char_get_val()
        * API returns a const pointer
        */
		const hap_val_t *cur_val = hap_char_get_val(hc);

		
		// TODO: read POT then assign to new_vale
		hap_val_t new_val;
		if (cur_val->i == 1)
		{
			ESP_LOGD(TAG, "Received Read where current value is ON");
			new_val.i = 0;
		}
		else
		{
			ESP_LOGD(TAG, "Received Read where current value is OFF");
			new_val.i = 1;
		}
		hap_char_update_val(hc, &new_val);
		*status_code = HAP_STATUS_SUCCESS;
	} else {
		ESP_LOGD(TAG, "Received Read where characteristic is unknown");
	}
	return HAP_SUCCESS;
}

/* A callback for handling a write on the "On" characteristic of Fan. */
static int bulb_write(
	hap_write_data_t write_data[], 
	int count, 
	void *serv_priv, 
	void *write_priv
) {

	ESP_LOGI(TAG, "--------------  Fan Write called with %d chars", count);

	if (hap_req_get_ctrl_id(write_priv))
	{
		ESP_LOGI(TAG, "Received write from %s", hap_req_get_ctrl_id(write_priv));
	}

	int i, ret = HAP_SUCCESS;
	hap_write_data_t *write;
	for (i = 0; i < count; i++)
	{
		write = &write_data[i];
		const char* char_type = hap_char_get_type_uuid(write->hc);
		bool state_changed = false;

		if (!strcmp(char_type, HAP_CHAR_UUID_HUE))
		{
			hue = map(
				write->val.f, 
				0, 360, 
				0, 65535
			);
			ESP_LOGI(TAG, "Received Write. HAP_CHAR_UUID_HUE %f %d", write->val.f, hue);
			state_changed = true;
		}
		if (!strcmp(char_type, HAP_CHAR_UUID_SATURATION))
		{
			saturation = map(
				write->val.f, 
				0, 100, 
				0, 255
			);
			ESP_LOGI(TAG, "Received Write. HAP_CHAR_UUID_SATURATION %f %f", write->val.f, saturation);
			state_changed = true;
		}
		if (!strcmp(char_type, HAP_CHAR_UUID_BRIGHTNESS))
		{
			brightness = map(
				write->val.u, 
				0, 100, 
				0, 255
			);
			ESP_LOGI(TAG, "Received Write. HAP_CHAR_UUID_BRIGHTNESS%d %d", write->val.u, brightness);
			state_changed = true;
		}
		if (!strcmp(char_type, HAP_CHAR_UUID_ON))
		{
			ESP_LOGI(TAG, "Received Write. HAP_CHAR_UUID_ON %s", write->val.b ? "true" : "false");
			is_bulb_on = write->val.b;
			state_changed = true;
		}

		if (state_changed) 
		{
			ESP_LOGD(TAG, "--- state_changed. %d %d %d", hue, saturation, brightness);
			if(is_bulb_on) {
				ESP_LOGD(TAG, "--- Received Write on. HAP_CHAR_UUID_ON %d %d %d", hue, saturation, brightness);
				writeHSV(hue, saturation, brightness);
			} else {
				ESP_LOGD(TAG, "Received Write off. HAP_CHAR_UUID_ON");
				writeHSV(0, 0, 0);
			}

			hap_char_update_val(write->hc, &(write->val));
			*(write->status) = HAP_STATUS_SUCCESS;

			// // TODO: Write values into EEPROM
			// EEPROM.writeByte(flash_address, flash_counter);
			// EEPROM.commit();
		} 
		else 
		{
			ESP_LOGD(TAG, "--- state_changed == false for characteristic UUID type %s", char_type);
			*(write->status) = HAP_STATUS_RES_ABSENT;
		}
	}
	return ret;
}

static int fan_read(
	hap_char_t *hc, 
	hap_status_t *status_code, 
	void *serv_priv, 
	void *read_priv
) {
	ESP_LOGI(TAG, "--------------  Fan Read called");

	if (hap_req_get_ctrl_id(read_priv))
	{
		ESP_LOGI(TAG, "Received read from %s", hap_req_get_ctrl_id(read_priv));
	}

	// if (!strcmp(hap_char_get_type_uuid(hc), HAP_CHAR_UUID_HUE))
	// {
	// 	/* Read the current value, toggle it and set the new value.
    //     * A separate variable should be used for the new value, as the hap_char_get_val()
    //     * API returns a const pointer
    //     */
	// 	const hap_val_t *cur_val = hap_char_get_val(hc);

		
	// 	// TODO: read POT then assign to new_vale
	// 	hap_val_t new_val;
	// 	if (cur_val->i == 1)
	// 	{
	// 		ESP_LOGD(TAG, "Received Read where current value is ON");
	// 		new_val.i = 0;
	// 	}
	// 	else
	// 	{
	// 		ESP_LOGD(TAG, "Received Read where current value is OFF");
	// 		new_val.i = 1;
	// 	}
	// 	hap_char_update_val(hc, &new_val);
	// 	*status_code = HAP_STATUS_SUCCESS;
	// } else {
		ESP_LOGD(TAG, "Received Read where characteristic is unknown");
	// }
	return HAP_SUCCESS;
}

/* A callback for handling a write on the "On" characteristic of Fan. */
static int fan_write(
	hap_write_data_t write_data[], 
	int count, 
	void *serv_priv, 
	void *write_priv
) {

	ESP_LOGI(TAG, "--------------  Fan Write called with %d chars", count);

	if (hap_req_get_ctrl_id(write_priv))
	{
		ESP_LOGI(TAG, "Received write from %s", hap_req_get_ctrl_id(write_priv));
	}

	int i, ret = HAP_SUCCESS;
	hap_write_data_t *write;
	for (i = 0; i < count; i++)
	{
		write = &write_data[i];
		const char* char_type = hap_char_get_type_uuid(write->hc);
		bool state_changed = false;

		if (!strcmp(char_type, HAP_CHAR_UUID_ACTIVE))
		{
			ESP_LOGI(TAG, "Received Write. HAP_CHAR_UUID_ACTIVE %s", write->val.b ? "true" : "false");
			is_fan_on = write->val.b;
			state_changed = true;
		}
		if (!strcmp(char_type, HAP_CHAR_UUID_ROTATION_SPEED))
		{
			fan_speed = (uint16_t)write->val.f;
			stepper_delta = fan_speed - stepper_angle;
			// stepper_angle will be updated when we write

			ESP_LOGI(TAG, "Received Write. HAP_CHAR_UUID_ROTATION_SPEED %f %d", write->val.f, fan_speed);
			state_changed = true;
		}

		if (state_changed) 
		{
			ESP_LOGD(TAG, "--- state_changed. %d", fan_speed);
			writeFan(
				is_fan_on, 
				is_fan_on ? fan_speed : 0
			);
			hap_char_update_val(write->hc, &(write->val));
			*(write->status) = HAP_STATUS_SUCCESS;

			// TODO: Write values into EEPROM
			// EEPROM.writeByte(flash_address, flash_counter);
			// EEPROM.commit();
		} 
		else 
		{
			ESP_LOGD(TAG, "--- state_changed == false for characteristic UUID type %s", char_type);
			*(write->status) = HAP_STATUS_RES_ABSENT;
		}
	}
	return ret;
}

// ------------ Global Functions (Arduino) ------------

void setup()
{
	Serial.begin(115200);

	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED)
	{
		delay(1000);
		ESP_LOGD(TAG, "Establishing connection to WiFi..");
	}
	ESP_LOGD(TAG, "Connected to network.");

	// *** EEPROM
	EEPROM.begin(EEPROM_SIZE);

	// TODO: EEPROM / Flash. Read each global var at boot
	// uint8_t flash_counter = EEPROM.read(flash_address);

	// *** Begin servo setup
	// // Allow allocation of all timers. Not sure this is necessary
	// ESP32PWM::allocateTimer(0);
	// ESP32PWM::allocateTimer(1);
	// ESP32PWM::allocateTimer(2);
	// ESP32PWM::allocateTimer(3);
	servo.setPeriodHertz(50);
	servo.attach(servo_pin);

	hap_acc_t *accessory;
	hap_serv_t *fan_service;
	hap_serv_t *bulb_service;
	hap_serv_t *shades_service;

	/* Configure HomeKit core to make the Accessory name (and thus the WAC SSID) unique,
     * instead of the default configuration wherein only the WAC SSID is made unique.
     */
	hap_cfg_t hap_cfg;
	hap_get_config(&hap_cfg);
	hap_cfg.unique_param = UNIQUE_NAME;
	hap_set_config(&hap_cfg);

	/* Initialize the HAP core */
	hap_init(HAP_TRANSPORT_WIFI);

	/* Initialise the mandatory parameters for Accessory which will be added as
     * the mandatory services internally
     */
	hap_acc_cfg_t cfg = {
		.name = "MotorFan",
		.model = "Espressif",
		.manufacturer = "MotorFan",
		.serial_num = "223344556677",
		.fw_rev = "0.0.1",
		.hw_rev = NULL,
		.pv = "1.1.0",
		.cid = HAP_CID_FAN,
		.identify_routine = identify,
	};

	/* Create accessory object */
	accessory = hap_acc_create(&cfg);

	/* Add Product Data */
	uint8_t product_data[] = {'E', 'S', 'P', '3', '2', 'K', 'I', 'T'};
	hap_acc_add_product_data(accessory, product_data, sizeof(product_data));

	// Lightbulb Service
	// Service: 
	// 	* HAP_SERV_UUID_LIGHTBULB "43"
	//
	// Required Characteristics:
	// * On (page 188)
	// 		* HAP_CHAR_UUID_ON "25"
	// 
	// Optional Characteristics:
	// * Name (page 187)
	// 		* HAP_CHAR_UUID_NAME "23"
	// * Brightness (page 178)
	// 		* HAP_CHAR_UUID_BRIGHTNESS "8"
	// * Hue (page 183)
	// 		* HAP_CHAR_UUID_HUE "13"
	// * Saturation (page 190)
	// 		* HAP_CHAR_UUID_SATURATION "2F"
	// * Color Temperature (page 238)
	// 		* HAP_CHAR_UUID_COLOR_TEMPERATURE "CE"
	//
	bulb_service = hap_serv_lightbulb_create(false);
	hap_serv_add_char(bulb_service, hap_char_name_create("My RGB"));
	hap_serv_add_char(bulb_service, hap_char_hue_create(0));
	hap_serv_add_char(bulb_service, hap_char_brightness_create(0));
	hap_serv_add_char(bulb_service, hap_char_saturation_create(0));
	hap_serv_set_write_cb(bulb_service, bulb_write);
	hap_serv_set_read_cb(bulb_service, bulb_read);
	hap_acc_add_serv(accessory, bulb_service);

	// Fan v2 Service
	// Service: 
	// 	* HAP_SERV_UUID_FAN_V2 "B7"
	//
	// Required Characteristics:
	// 	* Active (page 232)
	// 		* HAP_CHAR_UUID_ACTIVE "B0"
	// 
	// Optional Characteristics:
	// * Name (page 187)
	//		* HAP_CHAR_UUID_NAME "23"
	// * Current Fan State (page 232)
	// 		* HAP_CHAR_UUID_CURRENT_FAN_STATE "AF"
	// * Target Fan State (page 231)
	// 		* HAP_CHAR_UUID_TARGET_FAN_STATE "BF"
	// * Rotation Direction (page 189)
	// 		* HAP_CHAR_UUID_ROTATION_DIRECTION "28"
	// * Rotation Speed (page 189)
	// 		* HAP_CHAR_UUID_ROTATION_SPEED "29"
	// * Swing Mode (page 233)
	// 		* HAP_CHAR_UUID_SWING_MODE "B6"
	// * Lock Physical Controls (page 227)
	// 		* HAP_CHAR_UUID_LOCK_PHYSICAL_CONTROLS "A7"
	fan_service = hap_serv_fan_v2_create(false);
	hap_serv_add_char(fan_service, hap_char_name_create("My Fan"));
	hap_serv_add_char(fan_service, hap_char_rotation_speed_create(0));
	hap_serv_set_write_cb(fan_service, fan_write);
	hap_serv_set_read_cb(fan_service, fan_read);
	hap_acc_add_serv(accessory, fan_service);

	// Window Covering Service
	// Service:
	// 	* Window Covering (section 9.25)
	// 		* HAP_SERV_UUID_WINDOW_COVERING "8C"
	// 
	// Required Characteristics:
	// 	* Target Position (page 205)
	//		* HAP_CHAR_UUID_TARGET_POSITION "7C"
	// 	* Current Position (page 198)
	//		* HAP_CHAR_UUID_CURRENT_POSITION "6D"
	// 	* Position State (page 201)
	//		* HAP_CHAR_UUID_POSITION_STATE "72"
	// 
	// Optional Characteristics:
	// 	* Name (page 187) 
	//		* HAP_CHAR_UUID_NAME "23"
	// 	* Hold Position (page 199) 
	//		* HAP_CHAR_UUID_HOLD_POSITION "6F"
	// 	* Current Horizontal Tilt Angle (page 198) 
	// 		* HAP_CHAR_UUID_CURRENT_HORIZONTAL_TILT_ANGLE "6C"
	// 	* Target Horizontal Tilt Angle (page 205)
	//		* HAP_CHAR_UUID_TARGET_HORIZONTAL_TILT_ANGLE "7B"
	// 	* Current Vertical Tilt Angle (page 199)
	//		* HAP_CHAR_UUID_CURRENT_VERTICAL_TILT_ANGLE "6E"
	// 	* Target Vertical Tilt Angle (page 206)
	// 		* HAP_CHAR_UUID_TARGET_VERTICAL_TILT_ANGLE "7D"
	// 	* Obstruction Detected (page 188)
	//		* HAP_CHAR_UUID_OBSTRUCTION_DETECTED "24"
	// shades_service = hap_serv_window_covering_create(0, 0, 0)
	// hap_serv_add_char(shades_service, hap_char_name_create("My Fan"));
	// hap_serv_add_char(shades_service, hap_char_rotation_speed_create(0));
	// hap_serv_set_write_cb(shades_service, shades_write);
	// hap_serv_set_read_cb(shades_service, shades_read);
	// hap_acc_add_serv(accessory, shades_service);

	/* Add the Accessory to the HomeKit Database */
	hap_add_accessory(accessory);

	// TODO: if connected, render values from flash
	// otherwise render setup values.
	/* Query the controller count (just for information) */
	ESP_LOGI(
		TAG, 
		"Accessory is paired with %d controllers",
		hap_get_paired_controller_count()
	);

	/* Initializate hardware */
  	pixels.begin(); 

	/* Unique Setup code of the format xxx-xx-xxx. Default: 111-22-333 */
	// hap_set_setup_code("222-33-444");
	hap_set_setup_code("333-44-555");
	/* Unique four character Setup Id. Default: ES32 */
	hap_set_setup_id("ES32");

	/* After all the initializations are done, start the HAP core */
	hap_start();

	// Stepper Setup
	// TODO: Figure out best frequency for best motor
	// if (!AFMS.begin(1000)) {  // OR with a different frequency, say 1KHz
	if (AFMS.begin()) 
	{
		ESP_LOGD(TAG, "Motor Shield found.");
	}
	else 
	{
		ESP_LOGE(TAG, "Could not find Motor Shield. Check wiring.");
	}
  
	// TODO: instead of calling this, we can pass these values into 
	// hap_char_hue_create(...), etc...
	writeHSV(hue, saturation, brightness);
	// TODO: Do similar to the above, but for fan
}

void loop()
{
  	// adc_value = analogRead(adc_pin);
  	// adc_angle = map(
	// 	adc_value, 
	// 	0, adc_max, 
	// 	0, 180
	// );
}